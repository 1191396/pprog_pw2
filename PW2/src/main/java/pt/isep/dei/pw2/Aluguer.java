/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

/**
 *
 * @author Marcel e Matheus
 */
public interface Aluguer {
    
    //taxa cobrada pelo aluguer de um item
    static final double TAXA_ALUGUER = 0.05;
    /**
     * Metodo abstrato que calcula o valor de comercialização do item
     * @param valor
     * 
     */
    public double calcValAluguer(double valor);
}
