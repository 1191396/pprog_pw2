/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

/**
 *
 * @author Marcel e Matheus
 */
public interface Venda {

    //taxa cobrada pela venda de um item
    static final double TAXA_VENDA = 0.03;

    /**
     * Metodo abstrato que calcula o valor de comercialização do item
     * @param valor
     * 
     */
    public double calcValVenda(double valor);
    
    
}
