/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

/**
 *
 * @author Marcel e Matheus
 */
public class Automovel  {
    
    //Marca do automóvel
    private String marca;
    
    //Modelo do automóvel
    private String modelo;
    
    //Valor pretendido pelo automóvel
    private double valor;
    
    //Marca do automóvel por omissão
    private static final String MARCA_POR_OMISSAO = "em branco";
    
    //Modelo do automóvel por omissão
    private static final String MODELO_POR_OMISSAO = "em branco";

    //Valor pretendido por omissão
    private static final double VALOR_POR_OMISSAO = 0;

    
    
    /**
     * Cria uma instância automóvel com todos os valores por omissão
     */
    public Automovel(){
        marca = MARCA_POR_OMISSAO;
        modelo = MODELO_POR_OMISSAO;
    }
    
    /**
     * Cria uma instância automóvel com todos os parâmtros
     * @param marca
     * @param modelo
     * @param valor 
     */
    public Automovel(String marca, String modelo, double valor){
        this.marca = marca;
        this.modelo = modelo;
        this.valor = valor;
    }
    
    /**
     * Devolve a marca do automóvel
     * @return marca
     */
    public String getMarca(){
        return marca;
    }
    
    /**
     * Devolve o modelo do automóvel
     * @return modelo
     */
    public String getModelo(){
        return modelo;
    }
    
    /**
     * Devolve o valor pretendido do automóvel
     * @return valor
     */

    public double getValor(){
        return valor;
    }
    
    /**
     * Altera a marca do automóvel
     * @param marca 
     */
    public void setMarca (String marca){
        this.marca = marca;
    }
    
    /**
     * Altera o modelo do automóvel
     * @param modelo 
     */
    public void setModelo (String modelo){
        this.modelo = modelo;
    }
    
    /**
     * Altera o valor pretendido pelo automóvel
     * @param valor 
     */
    public void setValor (double valor){
        this.valor = valor;
    }
      /**
     * Reescreve o método toString da classe Object
     *
     * @return marca, modelo
     */
    @Override
    public String toString(){
        return "Automovel de Marca: "+marca+ " Modelo: " + modelo +"\nValor: ";
    }

  
 

}
