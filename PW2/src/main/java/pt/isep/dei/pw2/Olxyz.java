/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Marcel e Matheus
 */
public class Olxyz {

    public static void main(String[] args) {

        //Teste criação de objetos e envio de outros objetos como parametros
        Anunciante a1 = new Anunciante("Maria Rosa", new Endereco("Rua do teste", "4321-123", "Teste"));
        Anunciante a2 = new Anunciante("José Silva", new Endereco("Rua do teste", "4321-123", "Teste"));
        Anunciante a3 = new Anunciante("João Oliveira", new Endereco("Rua do teste", "4321-123", "Teste"));

        //Adiciona itens a anunciar
        a1.addTelemovel(new Telemovel("teste", 123));
        a1.addTelemovel(new Telemovel("teste", 123));
        a1.addAutomovel(new Automovel("Audi", "A4", 1700), Anunciante.Tipo.ALUGUER);
        a1.addApartamento(new Apartamento(0, 1000, new Endereco("Rua do teste", "4321-123", "Teste")));

        a2.addTelemovel(new Telemovel("teste", 123));
        a2.addAutomovel(new Automovel("Audi", "A4", 1700), Anunciante.Tipo.VENDA);
        a2.addApartamento(new Apartamento(0, 900, new Endereco("Rua do teste", "4321-123", "Teste")));

        a3.addApartamento(new Apartamento(0, 80, new Endereco("Rua do teste", "4321-123", "Teste")));
        a3.addApartamento(new Apartamento(0, 100, new Endereco("Rua do teste", "4321-123", "Teste")));

        List<Anunciante> anunciantes = new ArrayList<>();
        anunciantes.add(a1);
        anunciantes.add(a2);
        anunciantes.add(a3);

System.out.println("\n ################## \n");

//5.  Visualizar a quantidade de artigos disponíveis para aluguer na plataforma Olxyz;
        int qt = 0;
        for (int i = 0; i < anunciantes.size(); i++) {
            Anunciante a = anunciantes.get(i);
            qt += a.getContAluguer();
        }
        System.out.println("Há " + qt + " artigos disponíveis para alugar no Olxyz.");

System.out.println("\n ################## \n");

//6. retornar qual o total possível das suas vendas por cada anunciante
        for (int i = 0; i < anunciantes.size(); i++) {

            Anunciante a = anunciantes.get(i);

            if (a.getTotalVendas() != 0) {

                System.out.println("Anunciante:" + a.getNome() + "\n Morada: " + a.endereco + "\nTotal possível de vendas " + a.getTotalVendas() + "€");
            }
            System.out.println("-");
        }

System.out.println("\n ################## \n");

//7. retornar qual o Alugável mais caro por cada anunciante
        for (int i = 0; i < anunciantes.size(); i++) {

            Anunciante a = anunciantes.get(i);
            System.out.println("Anunciante:" + a.getNome() + "\nTipo: " + a.getTipoAluguer()
                    + "\nAluguer mais caro " + a.getMaiorAluguer() + "€"
                    + "\nLucro para a Olxyz: " + a.getLucroAluguer() + "€");

            System.out.println("-");
        }

    }
}
