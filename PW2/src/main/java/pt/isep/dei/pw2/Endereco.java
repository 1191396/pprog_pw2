/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

/**
 *
 * @author Marcel e Matheus
 */
public class Endereco {
    
        //Nome completo da rua do endereço
        private String rua;
        
        //Código postal do endereço
        private String zipcode;
        
        //Localidade do endereço
        private String localidade;
        //Nome da rua por omissão
        private static final String RUA_POR_OMISSAO = "em branco";
         //Zipcode por omissão
        private static final String ZIPCODE_POR_OMISSAO = "0000-00";
         //Localidade por omissão
        private static final String LOCALIDADE_POR_OMISSAO = "em branco";
        
    /**
     * Constroi uma instância endereço com todos os parâmetros por omissão
     */
    public Endereco(){
        rua = RUA_POR_OMISSAO ;
        zipcode = ZIPCODE_POR_OMISSAO;
        localidade = LOCALIDADE_POR_OMISSAO;
    }
    
    /**
     * Constroi uma instância endereço com todos os parâmetros
     * @param rua
     * @param zipcode
     * @param localidade
     * @param anunciante 
     */
    public Endereco (String rua, String zipcode, String localidade){
        this.rua = rua;
        this.zipcode = zipcode;
        this.localidade = localidade;
    }
    
    
    /**
     * Devolve a rua do endereço
     * @return rua
     */
    public String getRua(){
        return rua;
    }
    
    /**
     * Devolve o código postal do endereço
     * @return zipcode
     */
    public String getZipcode(){
        return zipcode;
    }
    
    /**
     * Devolve a localidade do endereço
     * @return localidade
     */
    public String getLocalidade(){
        return localidade;
    }
    
    /**
     * Altera a rua do endereço
     * @param rua 
     */
    public void setRua(String rua){
        this.rua = rua;
    }
    
    /**
     * Altera o código postal do endereço
     * @param zipcode 
     */
    public void setZipcode(String zipcode){
        this.zipcode = zipcode;
    }
    
    /**
     * Altera a localidade do endereço
     * @param localidade 
     */
    public void setLocalidade(String localidade){
        this.localidade = localidade;
    }
     /**
     * Reescreve o método toString da classe Object
     *
     * @return marca, modelo
     */
    @Override
    public String toString (){
        return "Rua: "+rua+"\n Código Postal: "+zipcode+" Localidade: "+localidade;
    }
    
}
