
package pt.isep.dei.pw2;

/**
 *
 * @author Marcel e Matheus
 */
public class Apartamento {
    
    //Área total do apartamento
    private float area;
    
    //Valor Pretendido (sem taxas)
    private double valor;
    
    //Endereço do apartamento
    Endereco endereco;
    
    
    //Area por omissão
    private static final float AREA_POR_DEFEITO = 0;
    
    //Valor pretendido(sem taxas) por omissão
    private static final double VALOR_POR_OMISSAO = 0;
    
    
    /**
     * Constroi instancia Apartamento com valores por omissão
     */
    public Apartamento(){
        area = AREA_POR_DEFEITO;
        valor = VALOR_POR_OMISSAO;
        endereco = new Endereco();
        
    }
    
    /**
     * Constroi instância Apartamento com todos os parâmetros
     * 
     * @param area
     * @param valor
     * @param endereco 
     */
    public Apartamento(float area, double valor, Endereco endereco){
        this.area = area;
        this.valor = valor; 
        this.endereco= new Endereco(endereco.getRua(),endereco.getZipcode(), endereco.getLocalidade());
    }
    
    /**
     * Devolve o valor da área total do apartamento
     * @return area
     */
    public float getArea(){
        return area;        
    }
    
    /**
     * Devolve valor pretendido(sem taxas)
     * @return valor
     */
    public double getValor(){
        return valor;
    }
    
    public Endereco getEndereco(){
        return endereco;
    }
    
    /**
     * Altera área total do apartamento
     * @param area 
     */
    public void setArea(float area){
        this.area = area;
    }
    
    /**
     * Altera valor pretendido (sem taxas)
     * @param valor 
     */
    public void setValor (double valor){
        this.valor = valor;
    }
    
    
    /**
     * Reescreve o método toString da classe Object
     * @return endereco, area, valor
     */
    @Override
    public String toString (){
        return endereco +"\n Área: "+area +" m^2"+'\n'+"Valor: "+ valor+"€";
    }
    
    /**
     * Implementa o método calcValAluguer da inteface Aluguel
     * @return 
     */


  
}
