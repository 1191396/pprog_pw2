/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Marcel e Matheus
 */
public class Anunciante implements Venda, Aluguer {

    //Nome completo do anunciante
    private String nome;
    //Contador total de Alugueis
    private int contAluguer = 0;
    //Contador de artigos em venda
    private int contVenda = 0;
    //variável de armazenamento do maior Aluguer
    private double maiorAluguer = 0;
    //variável de armazenamento do tipo de Aluguer
    private String tipoAluguer;
    //variável de armazenamento do total de vendas
    private double totalVendas = 0;
    //Limitador de artigos à venda por anunciante
    private int vendasMaximas = 2;
    //Limitador de artigos a alugar por anunciante
    private int aluguerMaximo = 3;
    //Endereço completo do anunciante
    Endereco endereco;
    //Nome do anunciante por omissão
    private static final String NOME_POR_OMISSAO = "em branco";
    //Contentor de objeto para itens vendáveis
    private List<Object> Alugueis;
    //Contentor de objeto para itens alugaveis
    private List<Object> Vendas;
    //Tipos de anuncio
    public static enum Tipo {
        ALUGUER,
        VENDA
    }

    /**
     * Constroi um anunciante com valores por omissão
     */
    public Anunciante() {
        nome = NOME_POR_OMISSAO;
        Endereco endereco = new Endereco();
        Alugueis = new ArrayList<>(3);
        Vendas = new ArrayList<>(2);
    }

    /**
     * Constroi um Anunciante com todos os parâmetros
     *
     * @param nome
     * @param endereco
     */
    public Anunciante(String nome, Endereco endereco) {
        this.nome = nome;
        this.endereco = endereco;
        Alugueis = new ArrayList<>(3);
        Vendas = new ArrayList<>(2);
    }

    /**
     * Devoleve o nome completo do anunciante
     *
     * @return nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve o contador de Alugueis
     *
     * @return contAluguer
     */
    public int getContAluguer() {
        return contAluguer;
    }
/**
     * Devolve o contador de Vendas
     *
     * @return contAluguer
     */
    public int getContVenda() {
        return contVenda;
    }
/**
     * Devolve a Lista de Alugueis
     *
     * @return List<Alugueis>
     */
    public List<Object> getAlugueis() {
        return Alugueis;
    }
/**
     * Devolve a Lista de Vendas
     *
     * @return List<Vendas>
     */
    public List<Object> getVendas() {
        return Vendas;
    }
/**
     * Devolve o Maior Aluguer registado
     *
     * @return maiorAluguer
     */
    public double getMaiorAluguer() {
        return maiorAluguer;
    }
/**
     * Devolve o total das Vendas
     *
     * @return totalVendas
     */
    public double getTotalVendas() {
        return totalVendas;
    }
/**
     * Devolve o tipo de Aluguer
     *
     * @return tipoAluguer
     */
    public String getTipoAluguer() {

        return tipoAluguer;
    }
/**
     * Devolve o Lucro de aluguer para a plataforma
     *
     * @return 
     */
    public double getLucroAluguer() {

        return maiorAluguer * TAXA_ALUGUER;
    }

    /**
     * Altera o nome do anunciante
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }


    /**
     * Reescreve o método toString da classe Object
     *
     * @return nome, endereco
     */
    @Override
    public String toString() {
        return "Nome:" + nome + '\n' + endereco;
    }

    
    /**
     * Metodo de Instanciamento de um anuncio do tipo 'Telemovel'
     * @param tele 
     */
    public void addTelemovel(Telemovel tele) {

        if (vendasMaximas != 0) {
            Vendas.add(new Telemovel(tele.getDesignacao(), tele.getValor()));
            contVenda++;
            totalVendas += tele.getValor();
            vendasMaximas--;
        }
    }
  /**
     * Metodo de Instanciamento de um anuncio do tipo 'Apartamento'
     * @param ap 
     */
    public void addApartamento(Apartamento ap) {
        if (aluguerMaximo != 0) {
            Alugueis.add(new Apartamento(ap.getArea(), ap.getValor(), ap.getEndereco()));
            contAluguer++;

            if (maiorvalor(ap.getValor(), maiorAluguer) == true) {

                this.tipoAluguer = "Apartamento";
            }
            aluguerMaximo--;
        }
    }
 /**
  * Metodo de Instanciamento de um anuncio do tipo 'Automovel'
  * @param at
  * @param tp 
  */
    public void addAutomovel(Automovel at, Tipo tp) {
        switch (tp) {
            case ALUGUER:
                if (aluguerMaximo != 0) {
                    Alugueis.add(new Automovel(at.getMarca(), at.getModelo(), at.getValor()));
                    contAluguer++;
                    if (maiorvalor(at.getValor(), maiorAluguer) == true) {

                        this.tipoAluguer = "Automovel";
                    }
                    aluguerMaximo--;
                }
                break;
            case VENDA:
                if (vendasMaximas != 0) {
                    Vendas.add(new Automovel(at.getMarca(), at.getModelo(), at.getValor()));
                    contVenda++;
                    totalVendas += at.getValor();
                    vendasMaximas--;
                }
                break;

        }
    }
/**
 * Método de comparacao de dois valores e possível substituicao 
 * @param valor
 * @param maior
 * @return 
 */
    public boolean maiorvalor(double valor, double maior) {
        if (valor > maior) {
            this.maiorAluguer = valor;
            return true;
        }
        return false;
    }
/**
 * Método de calculo do valor das vendas já com as respectivas taxas
 * @param valor
 * @return 
 */
    @Override
    public double calcValVenda(double valor) {
        double total = 0;
        total += (Venda.TAXA_VENDA+1) * valor;

        return total;
    }
/**
 * Método de calculo do valor dos Aluguers já com as respectivas taxas
 * @param valor
 * @return 
 */
    @Override
    public double calcValAluguer(double valor) {
        double total = 0;
        total += (Aluguer.TAXA_ALUGUER+1) * valor;

        return total;
    }

}
