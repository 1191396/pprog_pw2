/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

/**
 *
 * @author Marcel e Matheus
 */
public class Telemovel  {
    
    //Designação do telemóvel
    private String designacao;
    
    //Valor pretendido pelo telemóvel
    private double valor;
    
    //Designação do telemóvel por omissão
    private static final String DESIGNACAO_POR_OMISSAO = "em branco";
    
    //Valor pretendido pelo telemóvel por omissão
    private static final double VALOR_POR_OMISSAO = 0;
    
    /**
     * Constroi uma instância telemóvel com parâmetros por omissão
     */
    public Telemovel(){
        designacao = DESIGNACAO_POR_OMISSAO;
        valor = VALOR_POR_OMISSAO;
    }
    
    /**
     * Controi uma instância telemóvel com todos os parâmetros
     * @param designacao
     * @param valor 
     */
    public Telemovel(String designacao, double valor){
        this.designacao = designacao;
        this.valor = valor;
    }
    
    /**
     * Devolve a designação do telemóvel
     * @return designação
     */
    public String getDesignacao(){
        return designacao;
    }
    
    /**
     * Devolve o valor pretendido pelo telemóvel
     * @return valor
     */
    public double getValor(){
        return valor;
    }
    
    /**
     * Altera a designação do telemóvel
     * @param designacao 
     */
    public void setDesignacao(String designacao){
        this.designacao = designacao;
    }
    
    /**
     * ALtera o valor do telemóvel
     * @param valor 
     */
    public void setValor(double valor){
        this.valor = valor;
    }
        /**
     * Reescreve o método toString da classe Object
     *
     * @return marca, modelo
     */
    @Override
    public String toString(){
        return "Telemóvel: "+designacao +"| Valor: "+valor+"\n";
    }
   
  
}
