/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.isep.dei.pw2;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Crow
 */
public class AnuncianteTest {
    
    public AnuncianteTest() {
    }
    Anunciante a = new Anunciante();
    
    
    
    
    
    @Test
    public void testMaiorValor() {
       double valor=2;
       double maior=1;
      boolean expResult=true;
      
      Anunciante instance = new Anunciante();
      
      boolean result=instance.maiorvalor(valor, maior);
        assertEquals(expResult, result);
    }
    @Test
    public void testMaiorValor2() {
       double valor=1;
       double maior=2;
      boolean expResult=false;
      
      Anunciante instance = new Anunciante();
      
      boolean result=instance.maiorvalor(valor, maior);
        assertEquals(expResult, result);
    }
     @Test
    public void testCalcValVenda() {
       double valor=100;
     
      double expResult=103;
      
      Anunciante instance = new Anunciante();
      
      double result=instance.calcValVenda(valor);
        assertEquals(expResult, result,0.0);
    }
     @Test
    public void testCalcValAluguer() {
       double valor=100;
     
      double expResult=105;
      
      Anunciante instance = new Anunciante();
      
      double result=instance.calcValAluguer(valor);
        assertEquals(expResult, result,0.0);
    }
    @Test
    public void testgetAlugueis() {
       
        List<Object> expResult = new ArrayList();
        
    
   List<Object> result = a.getAlugueis();
        assertEquals(expResult, result);
    }
     @Test
    public void testgetVendas() {
       
        List<Object> expResult = new ArrayList();
        
    
   List<Object> result = a.getVendas();
        assertEquals(expResult, result);
    }
    
    @Test
    public void testgetContAluguer() {
       
      int expResult=0;
    int result=a.getContAluguer();
        assertEquals(expResult, result);
    }
     @Test
    public void testgetContVenda() {
       
      int expResult=0;
    int result=a.getContVenda();
        assertEquals(expResult, result);
    }
    @Test
    public void testgetMaiorAluguer() {
       
      double expResult=0;
    double result=a.getMaiorAluguer();
        assertEquals(expResult, result,0.0);
    }
    @Test
    public void testgetTotalVendas() {
       
      double expResult=0;
    double result=a.getTotalVendas();
        assertEquals(expResult, result,0.0);
    }
    @Test
    public void testgetNome() {
       
      String expResult="em branco";
     String result=a.getNome();
        assertEquals(expResult, result);
    }
    @Test
    public void testsetNome() {
       
      String expResult="Teste Top";
     a.setNome("Teste Top");
     String result = a.getNome();
        assertEquals(expResult, result);
    }
    
    
}
